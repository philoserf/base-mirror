---
title: Base
author: Mark Ayers
documentclass: article
papersize: letter
...

# Base

> [all your base are belong to us][1]

Nothing to see here. -- Yet -- Move along. Move along.

- - - - -

- © 2017 by Mark Ayers -- a human being. License: [MIT License][2].

[1]: https://en.wikipedia.org/wiki/All_your_base_are_belong_to_us
[2]: LICENSE
